devices="lis3mdl mpu6050"

lis3mdl_scan_elements="magn_x magn_y magn_z"
lis3mdl_samplerate=40

mpu6050_scan_elements="accel_x accel_y accel_z anglvel_x anglvel_y anglvel_z timestamp"
mpu6050_samplerate=200
mpu6050_more_enable() {
	echo -n 0.000133090 > "${iiodev}/in_anglvel_scale"
}
