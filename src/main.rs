use std::io::Result;

use fusion::{
    fusion::Fusion,
    devsampler::DevSampler,
    samplers::{lis3mdl::*, mpu6050::*},
};

fn main() -> Result<()> {
    let lis_ds = DevSampler::from_dst(Lis3mdl::new(), "/dev/magnetometer")?;
    let mpu_ds = DevSampler::from_dst(Mpu6050::new(), "/dev/accel_gyro")?;

    Fusion::with_args([lis_ds, mpu_ds].into())
}
